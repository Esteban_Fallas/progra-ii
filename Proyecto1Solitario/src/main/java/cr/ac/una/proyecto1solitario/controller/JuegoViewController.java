/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.proyecto1solitario.controller;

import cr.ac.una.proyecto1solitario.Carta;
import cr.ac.una.proyecto1solitario.Partida;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Bounds;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author esteb
 */
public class JuegoViewController implements Initializable {

    @FXML
    private VBox VbColumna1;
    @FXML
    private VBox VbColumna2;
    @FXML
    private VBox VbColumna3;
    @FXML
    private VBox VbColumna4;
    @FXML
    private VBox VbColumna5;
    @FXML
    private VBox VbColumna6;
    @FXML
    private VBox VbColumna7;
    @FXML
    private VBox VbColumna8;
    @FXML
    private VBox VbColumna9;
    @FXML
    private VBox VbColumna10;
    @FXML
    private ImageView ivFondo;
    @FXML
    private AnchorPane root;
    @FXML
    private HBox HbMazo;
    @FXML
    private VBox VbColumna1_2;
    @FXML
    private VBox VbColumna2_2;
    @FXML
    private VBox VbColumna3_2;
    @FXML
    private VBox VbColumna4_2;
    @FXML
    private VBox VbColumna5_2;
    @FXML
    private VBox VbColumna6_2;
    @FXML
    private VBox VbColumna7_2;
    @FXML
    private VBox VbColumna8_2;
    @FXML
    private VBox VbColumna9_2;
    @FXML
    private VBox VbColumna10_2;
    @FXML
    private VBox VbColumnaMain1;
    @FXML
    private VBox VbColumnaMain2;
    @FXML
    private VBox VbColumnaMain3;
    @FXML
    private VBox VbColumnaMain4;
    @FXML
    private VBox VbColumnaMain5;
    @FXML
    private VBox VbColumnaMain6;
    @FXML
    private VBox VbColumnaMain7;
    @FXML
    private VBox VbColumnaMain8;
    @FXML
    private VBox VbColumnaMain9;
    @FXML
    private VBox VbColumnaMain10;
    Partida par1=new Partida("facil");
  
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
       ivFondo.fitHeightProperty().bind(root.widthProperty());
       ivFondo.fitWidthProperty().bind(root.widthProperty());
      
       VbColumnaMain1.setSpacing(-170);
       VbColumnaMain2.setSpacing(-170);
       VbColumnaMain3.setSpacing(-170);
       VbColumnaMain4.setSpacing(-170);
       VbColumnaMain5.setSpacing(-170);
       VbColumnaMain6.setSpacing(-170);
       VbColumnaMain7.setSpacing(-170);
       VbColumnaMain8.setSpacing(-170);
       VbColumnaMain9.setSpacing(-170);
       VbColumnaMain10.setSpacing(-170);

       par1.crearCasillas();
        par1.getTab().getCasillas()[0].setVboxColumna(VbColumna1);
        par1.getTab().getCasillas()[1].setVboxColumna(VbColumna2);
        par1.getTab().getCasillas()[2].setVboxColumna(VbColumna3);
        par1.getTab().getCasillas()[3].setVboxColumna(VbColumna4);
        par1.getTab().getCasillas()[4].setVboxColumna(VbColumna5);
        par1.getTab().getCasillas()[5].setVboxColumna(VbColumna6);
        par1.getTab().getCasillas()[6].setVboxColumna(VbColumna7);
        par1.getTab().getCasillas()[7].setVboxColumna(VbColumna8);
        par1.getTab().getCasillas()[8].setVboxColumna(VbColumna9);
        par1.getTab().getCasillas()[9].setVboxColumna(VbColumna10);

        par1.getTab().getCasillas()[0].setVboxColumna2(VbColumna1_2);
        par1.getTab().getCasillas()[1].setVboxColumna2(VbColumna2_2);
        par1.getTab().getCasillas()[2].setVboxColumna2(VbColumna3_2);
        par1.getTab().getCasillas()[3].setVboxColumna2(VbColumna4_2);
        par1.getTab().getCasillas()[4].setVboxColumna2(VbColumna5_2);
        par1.getTab().getCasillas()[5].setVboxColumna2(VbColumna6_2);
        par1.getTab().getCasillas()[6].setVboxColumna2(VbColumna7_2);
        par1.getTab().getCasillas()[7].setVboxColumna2(VbColumna8_2);
        par1.getTab().getCasillas()[8].setVboxColumna2(VbColumna9_2);
        par1.getTab().getCasillas()[9].setVboxColumna2(VbColumna10_2);        
        par1.iniciarPartida(root);
        

         
    }    

    @FXML
    private void onActionRepartir10cartas(MouseEvent event) {
        if(par1.repartir10cartas()==104)
        {
            HbMazo.setDisable(true);
        }
    }

 

   



    
}
