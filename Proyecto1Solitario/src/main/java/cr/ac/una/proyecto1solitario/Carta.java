/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.proyecto1solitario;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author ALBERTH
 */
public class Carta {
    private String tipo; // bastos_____corazones____flores____diamantes
    private int numero;
    private ImageView imVi=new ImageView();;
    private ImageView imViTapa=new ImageView();
    private float coorX;
    private float coorY;
    private String estado; // bocaAbajo____bocaArriba____enBaraja____recogida
    private int numeroColumna;

    public Carta(String tipo, int numero, float coorX, float coorY, String estado) {
        this.tipo = tipo;
        this.numero = numero;
        this.coorX = coorX;
        this.coorY = coorY;
        this.estado=estado;
        selectImagenTapa();
        selectImagen();
        
    }
    
  
    public void selectImagenTapa()
    {
        Image im=new Image("/cr/ac/una/proyecto1solitario/resources/tapa.png");
        imViTapa.setImage(im);
        imViTapa.setFitWidth(125);
        imViTapa.setFitHeight(182);
    }

    public int getNumeroColumna() {
        return numeroColumna;
    }

    public void setNumeroColumna(int numeroColumna) {
        this.numeroColumna = numeroColumna;
    }
    
  
  
    public void selectImagen()
    {
       if(numero==1)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/ace_of_spades.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/ace_of_hearts.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/ace_of_clubs.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/ace_of_diamonds.png");
               imVi.setImage(im);
           }            
       }
       if(numero==2)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/2_of_spades.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/2_of_hearts.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/2_of_clubs.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/2_of_diamonds.png");
               imVi.setImage(im);
           }            
       }
       if(numero==3)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/3_of_spades.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/3_of_hearts.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/3_of_clubs.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/3_of_diamonds.png");
               imVi.setImage(im);
           }              
       }
       if(numero==4)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/4_of_spades.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/4_of_hearts.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/4_of_clubs.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/4_of_diamonds.png");
               imVi.setImage(im);
           }              
       }
       if(numero==5)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/5_of_spades.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/5_of_hearts.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/5_of_clubs.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/5_of_diamonds.png");
               imVi.setImage(im);
           }              
       }
       if(numero==6)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/6_of_spades.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/6_of_hearts.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/6_of_clubs.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/6_of_diamonds.png");
               imVi.setImage(im);
           }             
       }
       if(numero==7)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/7_of_spades.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/7_of_hearts.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/7_of_clubs.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/7_of_diamonds.png");
               imVi.setImage(im);
           }              
       }
       if(numero==8)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/8_of_spades.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/8_of_hearts.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/8_of_clubs.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/8_of_diamonds.png");
               imVi.setImage(im);
           }              
       }
       if(numero==9)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/9_of_spades.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/9_of_hearts.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/9_of_clubs.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/9_of_diamonds.png");
               imVi.setImage(im);
           }             
       }
       if(numero==10)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/10_of_spades.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/10_of_hearts.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/10_of_clubs.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/10_of_diamonds.png");
               imVi.setImage(im);
           }            
       }
       if(numero==11)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/jack_of_spades2.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/jack_of_hearts2.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/jack_of_clubs2.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/jack_of_diamonds2.png");
               imVi.setImage(im);
           }               
       }
       if(numero==12)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/queen_of_spades2.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/queen_of_hearts2.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/queen_of_clubs2.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/queen_of_diamonds2.png");
               imVi.setImage(im);
           }               
       }
       if(numero==13)
       {
           if(tipo.equals("bastos"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/king_of_spades2.png");
               imVi.setImage(im);
           }
           if(tipo.equals("corazones"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/king_of_hearts2.png");
               imVi.setImage(im);
           }           
           if(tipo.equals("flores"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/king_of_clubs2.png");
               imVi.setImage(im);
           }  
           if(tipo.equals("diamantes"))
           {
               Image im=new Image("/cr/ac/una/proyecto1solitario/resources/king_of_diamonds2.png");
               imVi.setImage(im);
           }             
       }       
    }
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public ImageView getImVi() {
        return imVi;
    }

    public void setImVi(ImageView imVi) {
        this.imVi = imVi;
    }

    public float getCoorX() {
        return coorX;
    }

    public void setCoorX(float coorX) {
        this.coorX = coorX;
    }

    public float getCoorY() {
        return coorY;
    }

    public void setCoorY(float coorY) {
        this.coorY = coorY;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ImageView getImViTapa() {
        return imViTapa;
    }

    public void setImViTapa(ImageView imViTapa) {
        this.imViTapa = imViTapa;
    }
    
}
