/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.proyecto1solitario;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.layout.VBox;

/**
 *
 * @author ALBERTH
 */
public class Casilla {
    private List<Carta> cartasBocaArriba=new ArrayList<Carta>();
    private List<Carta> cartasBocaAbajoL=new ArrayList<Carta>();
    private VBox vboxColumna;
    private VBox vboxColumna2;
    private int coorX;
    private int coorY;

    public List<Carta> getCartasBocaAbajoL() {
        return cartasBocaAbajoL;
    }

    public void setCartasBocaAbajoL(List<Carta> cartasBocaAbajoL) {
        this.cartasBocaAbajoL = cartasBocaAbajoL;
    }
    
    

    public Casilla(VBox vboxColumna) {
        this.vboxColumna = vboxColumna;
    }
    public Casilla() {
        
    }    

    public VBox getVboxColumna2() {
        return vboxColumna2;
    }

    public void setVboxColumna2(VBox vboxColumna2) {
        this.vboxColumna2 = vboxColumna2;
        this.vboxColumna2.setSpacing(-140);
    }


    public VBox getVboxColumna() {
        return vboxColumna;
    }

    public void setVboxColumna(VBox vboxColumna) {
        this.vboxColumna = vboxColumna;
        this.vboxColumna.setSpacing(-170);


    }

    public List<Carta> getCartasBocaArriba() {
        return cartasBocaArriba;
    }

    public void setCartasBocaArriba(List<Carta> cartasBocaArriba) {
        this.cartasBocaArriba = cartasBocaArriba;
    }
    
    
}
