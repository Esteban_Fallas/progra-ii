/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.proyecto1solitario.controller;

import cr.ac.una.proyecto1solitario.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author esteb
 */
public class AcercaDeViewController extends Application implements Initializable {

    @FXML
    private AnchorPane root;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    
    
    public void start(Stage stage) throws IOException{
        Parent root2 = FXMLLoader.load(getClass().getResource("/cr/ac/una/proyecto1solitario/view/InicioView.fxml"));
        
        Scene scene = new Scene(root2);
        stage.setScene(scene);
        stage.show();
        
    }
    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("InicioView");
    }
}
