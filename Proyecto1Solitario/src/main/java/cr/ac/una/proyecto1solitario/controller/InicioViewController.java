/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.proyecto1solitario.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import cr.ac.una.proyecto1solitario.util.FlowController;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ALBERTH
 */
public class InicioViewController extends Controller implements Initializable {

    @FXML
    private JFXButton btnAcercaDe;
    @FXML
    private JFXButton btnSalir;
    @FXML
    private VBox VboxContainer;
    @FXML
    private JFXButton btnNuevoJuego;
    @FXML
    private JFXDrawer jfxDrawer;


   
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
//     

        
        

    }    

    @Override
    public void initialize() {
        
    }

    @FXML
    private void onActionAcercaDe(ActionEvent event) {
                 try{
            
            VBox vbox =  FXMLLoader.load(getClass().getResource("/cr/ac/una/proyecto1solitario/view/AcercaDeView.fxml"));
            jfxDrawer.setSidePane(vbox);
            
            
        }
        catch(Exception ex){
                Logger.getLogger(InicioViewController.class.getName()).log(Level.SEVERE,"Error");
                }
    btnAcercaDe.addEventHandler(MouseEvent.MOUSE_CLICKED, (e)->{
        if(jfxDrawer.isOpened()){
        jfxDrawer.close();
        }else{
        jfxDrawer.open();
        }
        
        });
    }
    

    @FXML
    private void onActionSalir(ActionEvent event) {
     FlowController.getInstance().goMain();
      
    }

    @FXML
    private void OnActionNuevoJuego(ActionEvent event) {
//        FlowController.getInstance().goMain();
//                
    FlowController.getInstance().goViewInWindow("NombreView");
    ((Stage) btnNuevoJuego.getScene().getWindow()).close();
    }
    
    public void CerrarVentana(){
        
    }
}
