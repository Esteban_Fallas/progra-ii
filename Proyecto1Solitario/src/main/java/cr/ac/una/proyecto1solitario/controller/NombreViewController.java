/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.proyecto1solitario.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import cr.ac.una.proyecto1solitario.util.FlowController;
import cr.ac.una.proyecto1solitario.util.Mensaje;
import java.net.URL;
import java.util.ResourceBundle;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;


/**
 * FXML Controller class
 *
 * @author esteb
 */
public class NombreViewController extends Controller implements Initializable {

    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnAceptar;
    @FXML
    private JFXTextField txtNombre;
    @FXML
    private JFXRadioButton rbtnFacil;
    @FXML
    private JFXRadioButton rbtnMedio;
    @FXML
    private JFXRadioButton rbtnDificil;

    @FXML
    private AnchorPane root;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @Override
    public void initialize() {
       
    }

    @FXML
    private void OnActionCancelar(ActionEvent event) {
        
        ((Stage)btnCancelar.getScene().getWindow()).close();
        
    }

    @FXML
    private void OnActionAceptar(ActionEvent event) {
        try{
        if(txtNombre.getText()==null || txtNombre.getText().isEmpty()){
            new Mensaje().showModal(Alert.AlertType.ERROR, "Nombre no valido",
            (Stage) btnAceptar.getScene().getWindow(), "Es necesario un nombre para iniciar el juego ");
            
        
            
            }else{
                FlowController.getInstance().goMain();
                ((Stage) btnAceptar.getScene().getWindow()).close();
                ((Stage) btnAceptar.getScene().getWindow()).close();
                
            }
        
        }
    catch(Exception ex){
                Logger.getLogger(NombreViewController.class.getName()).log(Level.SEVERE,"Error");
                }
    }
    
    
    
}
