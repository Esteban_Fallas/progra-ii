/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.proyecto1solitario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author ALBERTH
 */
public class Partida {
    private Tablero tab=new Tablero();
    private String dificultadPartida;    // facil___medio____dificil
    private MazoCartas mazo1=new MazoCartas();
    private MazoCartas mazo2=new MazoCartas();
    private Carta baraja[]=new Carta[104];
    private ArrayList <Integer> list=new ArrayList<Integer>();

    int index=0;
    
    public Partida(String dificultadPartida) {
        this.dificultadPartida = dificultadPartida;

    }


    public void iniciarPartida(AnchorPane root)
    {
        crearMazos();
        llenarBaraja();
        generaAleatorios();
        repartirCartasBocaAbajo(root);
        repartir10cartas();
    }
    public void crearCasillas()
    {
        for(int i=0;i<10;i++)
        {
            Casilla cas=new Casilla();
            tab.insertarCasillas(cas, i);
        }
    }
    
    public void generaAleatorios()
    {
        for(int i=0;i<104;i++)
        {
            list.add(i); 
        }
        Collections.shuffle(list);
    }
    public void crearMazos()
    {
        if(dificultadPartida.equals("facil"))
        {
            mazo1.setTipo("un_palo");
            mazo2.setTipo("un_palo");           
        }
        if(dificultadPartida.equals("medio"))
        {
            mazo1.setTipo("dos_palos");
            mazo2.setTipo("dos_palos");           
        }
        if(dificultadPartida.equals("dificil"))
        {
            mazo1.setTipo("cuatro_palos");
            mazo2.setTipo("cuatro_palos");           
        }  
        mazo1.crear();
        mazo2.crear();
    }
    public void llenarBaraja()
    {
        int j=103;
        for(int i=0;i<52;i++)
        {
            baraja[i]=mazo1.getVectorMazo()[i];
            baraja[j]=mazo2.getVectorMazo()[i];
            j--;
        }
        
    }
    public void repartirCartasBocaAbajo(AnchorPane root)
    {  
        
        int cx=20;
        int cy=71;
        for(int i=0;i<5;i++)
        {
            for(int j=0;j<10;j++)
            {
                tab.getCasillas()[j].getCartasBocaAbajoL().add(baraja[list.get(index)]);       
                tab.getCasillas()[j].getCartasBocaAbajoL().get(i).setEstado("bocaAbajo");        
                tab.getCasillas()[j].getVboxColumna().getChildren().add(tab.getCasillas()[j].getCartasBocaAbajoL().get(i).getImViTapa());  
                index++; 
                cx+=173;
                System.out.println("carta boca abajo: "+tab.getCasillas()[j].getCartasBocaAbajoL().get(i).getNumero()+" de "+tab.getCasillas()[j].getCartasBocaAbajoL().get(i).getTipo());
  
                
//                Image im=new Image("/cr/ac/una/proyecto1solitario/resources/tapa.png");
//                ImageView iv=new ImageView();
//                iv.setImage(im);
//                iv.setLayoutX(1551);
//                iv.setLayoutY(840);
//                iv.setFitHeight(187);
//                iv.setFitWidth(125);
//                root.getChildren().add(iv); 
//                TranslateTransition tr=new TranslateTransition();
//                tr.setDuration(Duration.millis(1000));
//                double x=tab.getCasillas()[j].getCartasBocaAbajo()[i].getCoorX()-iv.getLayoutX();
//                double y=tab.getCasillas()[j].getCartasBocaAbajo()[i].getCoorY()-iv.getLayoutY();
//                tr.setToX(x);
//                tr.setToY(y);
//                tr.setNode(iv);           
//                tr.setCycleCount(1);
//                tr.play();
                
                if(i==4&&j==3)
                {
                    break;
                }                
            } 
            cx=24;
            cy+=12;
        }
        System.out.println(baraja[list.get(index)].getNumero()+baraja[list.get(index)].getTipo());
    }
    public int repartir10cartas()
    {
        for(int i=0;i<10;i++)
        {
            tab.getCasillas()[i].getCartasBocaArriba().add(baraja[list.get(index)]);
            int j=tab.getCasillas()[i].getCartasBocaArriba().size();
            tab.getCasillas()[i].getCartasBocaArriba().get(j-1).setEstado("bocaArriba");
            tab.getCasillas()[i].getCartasBocaArriba().get(j-1).setNumeroColumna(i);
            tab.getCasillas()[i].getVboxColumna2().getChildren().add(tab.getCasillas()[i].getCartasBocaArriba().get(j-1).getImVi());
            index++;
            tab.getCasillas()[i].getCartasBocaArriba().get(j-1).getImVi().setOnDragDetected(cartaOnDragDetectedEventHandler);
            tab.getCasillas()[i].getCartasBocaArriba().get(j-1).getImVi().setOnDragOver(cartaOnDragOverEventHandler);
            tab.getCasillas()[i].getCartasBocaArriba().get(j-1).getImVi().setOnDragDropped(cartaOnDragDroppedEventHandler);

        }
        return index;
    }
    List<Carta> cartasMover=new ArrayList<Carta>();
    List<Carta> listaCartasOrigen=new ArrayList<Carta>();
    EventHandler<MouseEvent> cartaOnDragDetectedEventHandler = new EventHandler<MouseEvent>() {
 
        @Override
        public void handle(MouseEvent event) {
            ImageView iv=(ImageView) event.getSource();
            System.out.println("DRAG DETECTED");
            for(int i=0;i<10;i++)
            {
                for(int j=tab.getCasillas()[i].getCartasBocaArriba().size()-1;j>=0;j--)
                {
                    if(tab.getCasillas()[i].getCartasBocaArriba().get(j).getImVi().equals(iv))
                    {
                        listaCartasOrigen=tab.getCasillas()[i].getCartasBocaArriba();
                        int k=j;
                        do
                        {
                            cartasMover.add(tab.getCasillas()[i].getCartasBocaArriba().get(k));                        
                            if(k+1==tab.getCasillas()[i].getCartasBocaArriba().size())
                            {
                                break;
                            }
                            else
                            {
                                if(tab.getCasillas()[i].getCartasBocaArriba().get(k+1).getNumero()+1!=
                                tab.getCasillas()[i].getCartasBocaArriba().get(k).getNumero()||
                                        tab.getCasillas()[i].getCartasBocaArriba().get(k+1).getTipo()!=
                                tab.getCasillas()[i].getCartasBocaArriba().get(k).getTipo())
                                {
                                    cartasMover.clear();
                                }
                            }
                            k++;
                        }while(tab.getCasillas()[i].getCartasBocaArriba().get(k).getNumero()+1==
                                tab.getCasillas()[i].getCartasBocaArriba().get(k-1).getNumero()&&
                                tab.getCasillas()[i].getCartasBocaArriba().get(k).getTipo()==
                                tab.getCasillas()[i].getCartasBocaArriba().get(k-1).getTipo());    
                    
                    Dragboard db=tab.getCasillas()[i].getCartasBocaArriba().get(j).getImVi().startDragAndDrop(TransferMode.ANY);
                    ClipboardContent cb=new ClipboardContent();
                    cb.putImage(tab.getCasillas()[i].getCartasBocaArriba().get(j).getImVi().getImage());
                    db.setContent(cb);
                    event.consume();
                    break;
                       
                    }
                }
            }
            for(int i=0;i<cartasMover.size();i++)
            {
                System.out.println(cartasMover.get(i).getNumero()+",");
            }
            
        }
    };
    EventHandler<DragEvent> cartaOnDragOverEventHandler = new EventHandler<DragEvent>() {
 
        @Override
        public void handle(DragEvent event) {
            if(event.getDragboard().hasImage())
            {
                event.acceptTransferModes(TransferMode.ANY);
            }
        }
    };
    EventHandler<DragEvent> cartaOnDragDroppedEventHandler = new EventHandler<DragEvent>() {
 
        @Override
        public void handle(DragEvent event) {
            System.out.println("DROPPED");
            ImageView iv=(ImageView) event.getSource();
            for(int i=0;i<10;i++)
            {
                if(!tab.getCasillas()[i].getCartasBocaArriba().isEmpty())
                {
                    int j=tab.getCasillas()[i].getCartasBocaArriba().size()-1;
                    if(tab.getCasillas()[i].getCartasBocaArriba().get(j).getImVi().equals(iv))
                    {
                        if(cartasMover!=null)
                        {
                            if(tab.getCasillas()[i].getCartasBocaArriba().get(j).getNumero()==cartasMover.get(0).getNumero()+1)
                            {
                                System.out.println("Se puede jaja");
                                moverCartas(tab.getCasillas()[i].getVboxColumna2(),tab.getCasillas()[i].getCartasBocaArriba());
                            }
                            else{System.out.println("No se puede");}
                            cartasMover.clear();                    
                            break;
                       }

                    } 
                }
                
            }
        }
    };
    public void moverCartas(VBox vbDestino,List<Carta> listaCartasDestino)
    {
        int j=cartasMover.get(0).getNumeroColumna();
        for(int i=0;i<cartasMover.size();i++)
        {
            vbDestino.getChildren().add(cartasMover.get(i).getImVi());
            listaCartasDestino.add(cartasMover.get(i));
            tab.getCasillas()[j].getCartasBocaArriba().remove(cartasMover.get(i));
        }
        cartasMover.clear(); 
    }
    public Tablero getTab() {
        return tab;
    }

    public void setTab(Tablero tab) {
        this.tab = tab;
    }
    
   
    
}
