/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.una.proyecto1solitario;

/**
 *
 * @author ALBERTH
 */
public class MazoCartas {
    private String tipo;
    private Carta vectorMazo[]=new Carta[52];

    public MazoCartas(String tipo) {
        this.tipo = tipo;
    }

    public MazoCartas() {
        this.tipo=" ";
    }
    
    public void crear(){
        int iv=0;
        if(tipo.equals("un_palo"))
        {
            for(int i=1;i<=4;i++)
            {
                for(int j=1;j<=13;j++)
                {
                    Carta cart=new Carta("bastos", j, 0, 0,"enBaraja");
                    vectorMazo[iv]=cart;
                    iv++;
                }
            }
        }
        if(tipo.equals("dos_palos"))
        {
            for(int i=1;i<=2;i++)
            {
                for(int j=1;j<=13;j++)
                {
                    Carta cart=new Carta("bastos", j, 0, 0,"enBaraja");
                    vectorMazo[iv]=cart;
                    iv++;                    
                }
            } 
            for(int i=1;i<=2;i++)
            {
                for(int j=1;j<=13;j++)
                {
                    Carta cart=new Carta("corazones", j, 0, 0,"enBaraja");
                    vectorMazo[iv]=cart;
                    iv++;                    
                }
            }            
        }
        if(tipo.equals("cuatro_palos"))
        {
            for(int i=1;i<=13;i++)
            {
                Carta cart=new Carta("bastos", i, 0, 0,"enBaraja");
                vectorMazo[iv]=cart;
                iv++;                              
            }
            for(int i=1;i<=13;i++)
            {
                Carta cart=new Carta("corazones", i, 0, 0,"enBaraja");
                vectorMazo[iv]=cart;
                iv++;                              
            }
            for(int i=1;i<=13;i++)
            {
                Carta cart=new Carta("flores", i, 0, 0,"enBaraja");
                vectorMazo[iv]=cart;
                iv++;                              
            }
            for(int i=1;i<=13;i++)
            {
                Carta cart=new Carta("diamantes", i, 0, 0,"enBaraja");
                vectorMazo[iv]=cart;
                iv++;                              
            } 
                       
        }        
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Carta[] getVectorMazo() {
        return vectorMazo;
    }

    public void setVectorMazo(Carta[] vectorMazo) {
        this.vectorMazo = vectorMazo;
    }
    

}
